using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZBGE.data
{

  public class EventContext
  {
    
    public EventTriggerHook hook;

    public TerrainType terrainType;

    public EventContext(EventTriggerHook hook, TerrainType type)
    {
      this.hook = hook;
      this.terrainType = type;
    }

  }

}
