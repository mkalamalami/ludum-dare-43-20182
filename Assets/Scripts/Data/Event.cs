using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using NaughtyAttributes;

namespace ZBGE.data
{
  [CreateAssetMenu(menuName = "Event")]
  public class Event : ScriptableObject
  {
    [ReorderableList]
    public EventTrigger[] trigger;

    [Header("Scouting")]

    public EventTest scoutingTest;

    [TextArea]
    public string scoutingSuccessText;

    [ReorderableList]
    public EventOutcome[] scoutingSuccessOutcomes;

    [TextArea]
    public string scoutingFailureText;

    [ReorderableList]
    public EventOutcome[] scoutingFailureOutcomes;

    [Header("Full event")]

    public string title;

    [TextArea]
    public string text;

    public Sprite illustration;

    [ReorderableList]
    public EventChoice[] choices;

  }

}