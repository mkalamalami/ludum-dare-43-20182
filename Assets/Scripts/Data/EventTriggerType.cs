using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZBGE.data
{

  public enum EventTriggerHook
  {

    MOVEMENT,
    FOOD_SEARCH,
    TRACK_SEARCH

  }

}
