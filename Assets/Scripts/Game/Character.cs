using UnityEngine;
using ZBGE.data;

namespace ZBGE.Game
{

  public class Character
  {

    public const int MAX_STAMINA = 4;

    public const int MAX_SANITY = 4;

    public CharacterData characterData;

    [Range(0, 4)]
    public int stamina = 4;

    [Range(0, 4)]
    public int sanity = 4;

    public Character(CharacterData characterData)
    {
      this.characterData = characterData;
    }

    public void AddStamina(int dStamina)
    {
      this.stamina = Mathf.Clamp(stamina + dStamina, 0, MAX_STAMINA);
    }

    public void AddSanity(int dSanity)
    {
      this.sanity = Mathf.Clamp(sanity + dSanity, 0, MAX_SANITY);
    }

    public int GetExplorationBonus()
    {
      return characterData.trait.explorationBonus + (characterData.trait.explorationSanityBonus ? Mathf.Max(0, sanity - 1) : 0);
    }

    public int GetCombatBonus()
    {
      return characterData.trait.combatBonus + (characterData.trait.combatStaminaBonus ? Mathf.Max(0, stamina - 1) : 0);
    }

    public string GetExplorationBonusTxt()
    {
      return characterData.trait.explorationBonus + (characterData.trait.explorationSanityBonus ? IntToStr(Mathf.Max(0, sanity - 1)) : "");
    }

    public string GetCombatBonusTxt()
    {
      return characterData.trait.combatBonus + (characterData.trait.combatStaminaBonus ? IntToStr(Mathf.Max(0, stamina - 1)) : "");
    }

    private string IntToStr(int value)
    {
      return (value > 0 ? "+" : "") + value;
    }
  }

}