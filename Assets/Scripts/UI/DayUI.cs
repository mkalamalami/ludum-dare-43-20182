﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using ZBGE.Game;

namespace ZBGE.UI
{
  public class DayUI : MonoBehaviour
  {
    public TextMeshProUGUI day;
    public Transform timeIndicator;

    private PlayerManager playerManager;
    private Animator playerManagerAnimator;
    private int currentTime = 0;
    private Vector3 target;
    private Vector3[] positions = { new Vector2(-120, -42), new Vector3(-45, -42), new Vector3(32, -42), new Vector3(114, -42) };

    void Start()
    {
      playerManager = FindObjectOfType<PlayerManager>();
      playerManagerAnimator = playerManager.GetComponent<Animator>();
      target = positions[0];
    }

    void Update()
    {
      float step = 10f * Time.deltaTime;
      timeIndicator.localPosition = Vector2.Lerp(timeIndicator.localPosition, target, step);
    }

    void FixedUpdate()
    {
      day.SetText("DAY " + playerManagerAnimator.GetInteger("day").ToString());
      if (currentTime != playerManagerAnimator.GetInteger("timeOfDay"))
      {
        currentTime = playerManagerAnimator.GetInteger("timeOfDay");
        target = positions[(int)(currentTime / 6)];
      }
    }
  }
}
