﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using ZBGE.Game;

namespace ZBGE.UI
{
  public class CharacterInfoUI : MonoBehaviour
  {
    public Image characterIcon;
    public TextMeshProUGUI abbreviation;
    public TextMeshProUGUI characterName;
    public TextMeshProUGUI characterTrait;
    public ProgressBarUI staminaLevel;
    public ProgressBarUI sanityLevel;

    public TextMeshProUGUI explorationBonus;
    public Image explorationBonusSanityImg;
    public TextMeshProUGUI combatBonus;
    public Image combatBonusStaminaImg;

    public GameObject infoPanel;

    public void ShowCharacter(Character character)
    {
      var characterData = character.characterData;

      characterIcon.sprite = characterData.portrait;
      abbreviation.SetText(characterData.trait.abbreviation);

      characterName.SetText(characterData.identity.title);
      characterTrait.SetText(characterData.trait.title);

      staminaLevel.maxValue = Character.MAX_STAMINA;
      staminaLevel.SetValue(character.stamina);
      sanityLevel.maxValue = Character.MAX_SANITY;
      sanityLevel.SetValue(character.sanity);
      explorationBonus.SetText(character.GetExplorationBonusTxt().ToString());
      explorationBonusSanityImg.gameObject.SetActive(character.characterData.trait.explorationSanityBonus);
      combatBonus.SetText(character.GetCombatBonusTxt().ToString());
      combatBonusStaminaImg.gameObject.SetActive(character.characterData.trait.combatStaminaBonus);

      infoPanel.SetActive(true);
    }
  }
}