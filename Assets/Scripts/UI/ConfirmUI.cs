﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using ZBGE.data;
using ZBGE.Game;
using TMPro;

namespace ZBGE.UI
{
  public class ConfirmUI : MonoBehaviour
  {

    public delegate void OnClose();

    public event OnClose OnCloseEvent;

    public TextMeshProUGUI title;

    public TextMeshProUGUI text;

    public void SetTitle(string title)
    {
      this.title.text = title;
    }
    public void SetText(string text)
    {
      this.text.text = text;
    }

    public void OnCloseClick()
    {
      PlayerManager playerManager = FindObjectOfType<PlayerManager>();
      playerManager.PlayClip(playerManager.clickSound);
      OnCloseEvent();
    }

  }

}