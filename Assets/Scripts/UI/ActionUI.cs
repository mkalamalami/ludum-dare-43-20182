﻿using System.Collections.Generic;
using UnityEngine;
using ZBGE.data;

using TMPro;

namespace ZBGE.UI
{
  public class ActionUI : MonoBehaviour
  {
    public delegate void ActionSelect(Action action);
    public event ActionSelect OnActionSelected;

    public ActionList availableActions;
    public ActionBtn actionBtnPrefab;

    public Transform actionsContent;

    public TextMeshProUGUI title;
    public TextMeshProUGUI legend;

    private Dictionary<Action, ActionBtn> actionButtons = new Dictionary<Action, ActionBtn>();

    public void Start()
    {
      UpdateUI();
    }

    public void SetVisible(bool visible)
    {
      this.gameObject.SetActive(visible);
    }

    public void SetTitle(string title)
    {
      this.title.text = title;
    }

    public void SetLegend(string legend)
    {
      this.legend.text = legend;
    }

    void UpdateUI()
    {
      // Disable all buttons
      foreach (var item in actionButtons.Values)
      {
        item.gameObject.SetActive(false);
      }

      // Enable or create new icons
      foreach (var item in availableActions.actions)
      {
        if (actionButtons.ContainsKey(item))
        {
          actionButtons[item].gameObject.SetActive(true);
        }
        else
        {
          actionButtons.Add(item, CreateActionBtn(item));
        }
      }
    }

    private ActionBtn CreateActionBtn(Action action)
    {
      ActionBtn button = GameObject.Instantiate(actionBtnPrefab, Vector3.zero, Quaternion.identity, actionsContent) as ActionBtn;
      button.Init(action);
      button.OnActionSelected += SelectAction;
      return button;
    }

    private void SelectAction(Action action)
    {
      if (OnActionSelected != null)
      {
        OnActionSelected(action);
      }
    }
  }
}
