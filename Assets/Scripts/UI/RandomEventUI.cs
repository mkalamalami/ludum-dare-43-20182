﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

using ZBGE.data;
using ZBGE.Game;
using TMPro;
using System;

namespace ZBGE.UI
{
  public class RandomEventUI : MonoBehaviour
  {

    public delegate void OnChoiceClick(int index);

    public event OnChoiceClick OnChoiceClickEvent;

    public TextMeshProUGUI title;

    public TextMeshProUGUI text;

    public Transform choiceButtonsRoot;

    public ChoiceButtonUI choiceButtonTemplate;

    public void Start()
    {
      choiceButtonTemplate.gameObject.SetActive(false);
    }

    public void SetTitle(string title)
    {
      this.title.text = title;
    }
    public void SetText(string text)
    {
      this.text.text = text;
    }

    public void SetChoices(EventChoice[] choices)
    {
      foreach (Transform child in choiceButtonsRoot)
      {
        Destroy(child.gameObject);
      }

      const int MARGIN = 30;
      int choiceIndex = 0;
      float choiceWidth = (choiceButtonTemplate.GetComponent<RectTransform>().sizeDelta.x - (choices.Length - 1) * MARGIN) / choices.Length;

      foreach (EventChoice choice in choices)
      {
        ChoiceButtonUI choiceUI = Instantiate(choiceButtonTemplate, choiceButtonTemplate.transform.position, Quaternion.identity, choiceButtonsRoot);
        choiceUI.Init(this, choiceIndex, choice.text, choice.test.useCombatBonus, choice.test.useExplorationBonus);

        RectTransform rect = choiceUI.GetComponent<RectTransform>();
        rect.anchoredPosition = new Vector2((choiceWidth + MARGIN) * choiceIndex, 0);
        rect.SetSizeWithCurrentAnchors(RectTransform.Axis.Horizontal, choiceWidth);

        choiceUI.gameObject.SetActive(true);

        choiceIndex++;
      }
    }

    public void OnChoiceButtonClick(int index)
    {
      PlayerManager playerManager = FindObjectOfType<PlayerManager>();
      playerManager.PlayClip(playerManager.clickSound);
      OnChoiceClickEvent(index);
    }
  }
}