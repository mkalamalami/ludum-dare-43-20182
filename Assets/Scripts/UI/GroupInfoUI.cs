﻿using System.Collections.Generic;
using TMPro;
using UnityEngine;

using ZBGE.Game;

namespace ZBGE.UI
{
  public class GroupInfoUI : MonoBehaviour
  {
    public TextMeshProUGUI membersCount;
    public TextMeshProUGUI rationsCount;
    public TextMeshProUGUI combat;
    public TextMeshProUGUI exploration;
    public TextMeshProUGUI membersLostCount;

    private GroupManager groupManager;

    void Start()
    {
      groupManager = GameObject.FindGameObjectWithTag("Player").GetComponent<GroupManager>();
      groupManager.group.OnGroupChange += GroupChange;
    }

    void FixedUpdate()
    {
      rationsCount.SetText(groupManager.group.rations.ToString());
      combat.SetText(groupManager.GetCombatValue().ToString());
      exploration.SetText(groupManager.GetExplorationValue().ToString());
    }

    public void GroupChange(List<Character> characters)
    {
      membersCount.SetText(characters.Count.ToString());
    }
  }
}