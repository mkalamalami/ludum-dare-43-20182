using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace ZBGE.map
{

  [ExecuteInEditMode]
  public class MapEditorComponent : MonoBehaviour
  {

    public GameObject tilePrefab;
    public GameObject emptyPrefab;
    public int width;
    public int height;

  }

}