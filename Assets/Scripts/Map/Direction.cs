using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace ZBGE.map
{

  public enum Direction
  {

    TOP = 0,
    TOP_TIGHT = 1,

    BOTTOM_RIGHT = 2,

    BOTTOM = 3,

    BOTTOM_LEFT = 4,

    TOP_LEFT = 5

  }

}