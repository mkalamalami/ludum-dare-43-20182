﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ZBGE.data;
using ZBGE.Game;
using ZBGE.UI;

public class CrashBehaviour : StateMachineBehaviour
{
  public Database database;
  public EventChoice[] choices;

  private UIManager uIManager;
  private GroupManager groupManager;
  private RandomEventUI randomEventUI;
  private ConfirmUI confirmUI;
  private Animator animator;


  // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
  override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
  {
    this.animator = animator;
    uIManager = FindObjectOfType<UIManager>();
    if (uIManager == null) Debug.LogError("Scene has no UI Manager");

    groupManager = GameObject.FindGameObjectWithTag("Player").GetComponent<GroupManager>();
    if (groupManager == null) Debug.LogError("Scene has no Player group Manager");

    uIManager.SetStartPanelVisible(true);

    uIManager.ConfigureRandomEventUI("The Crash", "Air Force One has been struck by lightning and is about to crash on an unknown island... \r\n" +
    "And there's not enough parachutes for everyone.\r\n\r\nAs the President, you're getting the first one. How should we split the rest?", choices);
    randomEventUI = uIManager.SetRandomEventUIVisible(true);
    randomEventUI.OnChoiceClickEvent += OnEventChoice;
  }

  private void OnEventChoice(int index)
  {
    uIManager.SetRandomEventUIVisible(false);
    randomEventUI.OnChoiceClickEvent -= OnEventChoice;

    if (index == 0)
    {
      groupManager.InitializeGroup(database, 0, 0);
    }
    else
    {
      groupManager.InitializeGroup(database, 2, 8);
    }

    uIManager.ConfigureConfirmUI("Into the unknown", choices[index].outcomeSuccessText);
    confirmUI = uIManager.SetConfirmUIVisible(true);
    confirmUI.OnCloseEvent += OnConfirmClose;
  }

  public void OnConfirmClose()
  {
    confirmUI = uIManager.SetConfirmUIVisible(false);
    confirmUI.OnCloseEvent -= OnConfirmClose;

    uIManager.SetStartPanelVisible(false);
    uIManager.NextDay(1);
    this.animator.SetInteger("day", 1);
  }

  // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
  //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
  //{
  //    
  //}

  // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
  //override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
  //{
  //    
  //}

  // OnStateMove is called right after Animator.OnAnimatorMove()
  //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
  //{
  //    // Implement code that processes and affects root motion
  //}

  // OnStateIK is called right after Animator.OnAnimatorIK()
  //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
  //{
  //    // Implement code that sets up animation IK (inverse kinematics)
  //}
}
