﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using ZBGE.Game;

public class VictoryBehaviour : StateMachineBehaviour
{

  override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
  {
    animator.GetComponent<PlayerManager>().BackToMenu();
  }

}
