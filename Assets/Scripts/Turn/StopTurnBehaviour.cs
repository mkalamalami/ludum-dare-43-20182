﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ZBGE.Game;
using ZBGE.UI;
using ZBGE.data;

public class StopTurnBehaviour : StateMachineBehaviour
{

  private Animator animator;

  private UIManager uiManager;

  override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
  {
    this.animator = animator;

    uiManager = FindObjectOfType<UIManager>();
    if (uiManager == null) Debug.LogError("Scene has no UI Manager");

    uiManager.SetCharactersActionPanelVisible(false);
    uiManager.SetGroupActionPanelVisible(false);
    uiManager.SetRandomEventUIVisible(false);
    uiManager.SetValidatePanelVisible(false);
    uiManager.groupManagerUI.CanSelectCharacters = false;

  }
  override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
  {
  }

}
