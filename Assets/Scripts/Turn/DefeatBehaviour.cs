﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

using ZBGE.Game;
public class DefeatBehaviour : StateMachineBehaviour
{
  override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
  {
    animator.GetComponent<PlayerManager>().BackToMenu();
  }
}
