﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ZBGE.UI;

public class AdvanceDayBehaviour : StateMachineBehaviour
{
  // OnStateEnter is called when a transition starts and the state machine starts to evaluate this state
  override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
  {
    UIManager uiManager = FindObjectOfType<UIManager>();
    if (uiManager == null) Debug.LogError("Scene has no UI Manager");

    int time = animator.GetInteger("timeOfDay");
    int day = animator.GetInteger("day");
    time += 6;
    if(time > 18){
      time = 0;
      day++;
      uiManager.NextDay(day);
    }
    FindObjectOfType<ZBGE.UI.Compass>().NewTurn();

    animator.SetInteger("timeOfDay", time);
    animator.SetInteger("day", day);
  }

  // OnStateUpdate is called on each Update frame between OnStateEnter and OnStateExit callbacks
  //override public void OnStateUpdate(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
  //{
  //    
  //}

  // OnStateExit is called when a transition ends and the state machine finishes evaluating this state
  override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
  {

  }

  // OnStateMove is called right after Animator.OnAnimatorMove()
  //override public void OnStateMove(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
  //{
  //    // Implement code that processes and affects root motion
  //}

  // OnStateIK is called right after Animator.OnAnimatorIK()
  //override public void OnStateIK(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
  //{
  //    // Implement code that sets up animation IK (inverse kinematics)
  //}
}
