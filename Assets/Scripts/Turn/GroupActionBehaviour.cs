﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

using ZBGE.Game;
using ZBGE.UI;
using ZBGE.data;

public class GroupActionBehaviour : StateMachineBehaviour
{

  private PlayerManager playerManager;

  private Animator animator;

  private UIManager uiManager;

  override public void OnStateEnter(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
  {
    this.animator = animator;
    playerManager = FindObjectOfType<PlayerManager>();
    if (playerManager == null) Debug.LogError("Scene has no player");

    playerManager.CanMoveGroup = true;
    playerManager.OnMoveEvent += OnGroupMove;

    uiManager = FindObjectOfType<UIManager>();
    if (uiManager == null) Debug.LogError("Scene has no UI Manager");

    uiManager.SetGroupActionPanelVisible(true);
    uiManager.OnActionSelected += OnActionSelected;

  }

  public void OnActionSelected(Action action)
  {
    playerManager.ExecuteAction(action);
    animator.SetTrigger("choseGroupAction");
  }

  public void OnGroupMove(GroupMovement group)
  {
    animator.SetTrigger("choseGroupAction");
  }

  override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex)
  {
    playerManager.CanMoveGroup = false;
    
    uiManager.OnActionSelected -= OnActionSelected;
    playerManager.OnMoveEvent -= OnGroupMove;

    uiManager.SetGroupActionPanelVisible(false);
  }

}
