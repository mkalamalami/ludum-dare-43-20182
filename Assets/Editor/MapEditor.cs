using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace ZBGE.map
{

  [CustomEditor(typeof(MapEditorComponent))]
  public class MapEditor : Editor
  {

    public static Vector2 tileOffsetI = new Vector2(1.0f, -1.732f);

    public static Vector2 tileOffsetJ = new Vector2(2.0f, 0);

    SerializedProperty tilePrefab;
    SerializedProperty emptyPrefab;
    SerializedProperty width;
    SerializedProperty height;

    void OnEnable()
    {
      tilePrefab = serializedObject.FindProperty("tilePrefab");
      emptyPrefab = serializedObject.FindProperty("emptyPrefab");
      width = serializedObject.FindProperty("width");
      height = serializedObject.FindProperty("height");
    }

    private void RefreshTextures(Transform transform) {
      foreach (Transform row in transform) {
        foreach (Transform tileTransform in row) {
          TileComponent tile = tileTransform.GetComponent<TileComponent>();
          if (tile != null) {
            tile.RefreshSprite();
          }
        }
      }
    }

    private void GenerateTerrain(Transform transform, int width, int height, GameObject emptyPrefab, GameObject tilePrefab)
    {
      int it = 0;
      while (transform.childCount > 0 && it++ < 1000)
      {
        GameObject.DestroyImmediate(transform.GetChild(0).gameObject);
      }
      if (it >= 1000)
      {
        Debug.LogError("Failed to clean terrain");
        return;
      }

      float unitWidth = width * Mathf.Max(Mathf.Abs(tileOffsetI.x), Mathf.Abs(tileOffsetJ.x));
      float unitHeight = height * Mathf.Max(Mathf.Abs(tileOffsetI.y), Mathf.Abs(tileOffsetJ.y));

      List<GameObject> rows = new List<GameObject>();
      for (int i = -width * 2; i < width * 2; i++)
      {
        GameObject row = PrefabUtility.InstantiatePrefab(emptyPrefab) as GameObject;
        row.name = "Row[" + i + "]";
        row.transform.parent = transform;
        row.transform.localPosition = Vector3.zero;
        for (int j = -height * 2; j < height * 2; j++)
        {
          Vector2 position = i * tileOffsetI + j * tileOffsetJ;
          if (position.x > -unitWidth/2 && position.x < unitWidth/2 && position.y > -unitHeight/2 && position.y < unitHeight/2)
          {
            GameObject tileObject = PrefabUtility.InstantiatePrefab(tilePrefab) as GameObject;
            tileObject.transform.parent = row.transform;
            tileObject.transform.localPosition = i * tileOffsetI + j * tileOffsetJ;
            tileObject.name = "Tile[" + i + ";" + j + "]";
            tileObject.isStatic = true;
            tileObject.transform.localScale = new Vector3(1, 1, 1) * Random.Range(0.95f, 0.99f);

            TileComponent tile = tileObject.GetComponent<TileComponent>();
            tile.i = i;
            tile.j = j;

            int sortingOrder = i - 100;
            tileObject.GetComponent<SpriteRenderer>().sortingOrder = sortingOrder;
          }
        }
        if (row.transform.childCount == 0)
        {
          DestroyImmediate(row);
        }
      }
    }

    public override void OnInspectorGUI()
    {
      serializedObject.Update();

      GUILayout.Label("Terrain settings", EditorStyles.boldLabel);
      tilePrefab.objectReferenceValue = EditorGUILayout.ObjectField("Tile prefab", tilePrefab.objectReferenceValue as GameObject, typeof(GameObject), false, new GUILayoutOption[] { }) as GameObject;
      emptyPrefab.objectReferenceValue = EditorGUILayout.ObjectField("Empty prefab", emptyPrefab.objectReferenceValue as GameObject, typeof(GameObject), false, new GUILayoutOption[] { }) as GameObject;
      width.intValue = EditorGUILayout.IntField("Width", width.intValue);
      height.intValue = EditorGUILayout.IntField("Height", height.intValue);

      GUILayout.Label("Actions", EditorStyles.boldLabel);
      if (GUILayout.Button("Refresh textures"))
      {
        RefreshTextures((serializedObject.targetObject as MapEditorComponent).transform);
      }

      GUILayout.Label("Dangerous actions", EditorStyles.boldLabel);
      if (GUILayout.Button("Generate"))
      {
        GenerateTerrain((serializedObject.targetObject as MapEditorComponent).transform,
          width.intValue,
          height.intValue,
          emptyPrefab.objectReferenceValue as GameObject,
          tilePrefab.objectReferenceValue as GameObject);
      }
      serializedObject.ApplyModifiedProperties();
    }



  }

}